'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _idx = require('idx');

var _idx2 = _interopRequireDefault(_idx);

var _AdSlot = require('./AdSlot');

var _AdSlot2 = _interopRequireDefault(_AdSlot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable react/prefer-stateless-function */
var AdSlotWithWally = function (_Component) {
  (0, _inherits3.default)(AdSlotWithWally, _Component);

  function AdSlotWithWally() {
    (0, _classCallCheck3.default)(this, AdSlotWithWally);
    return (0, _possibleConstructorReturn3.default)(this, (AdSlotWithWally.__proto__ || (0, _getPrototypeOf2.default)(AdSlotWithWally)).apply(this, arguments));
  }

  (0, _createClass3.default)(AdSlotWithWally, [{
    key: 'render',
    value: function render() {
      if ((0, _idx2.default)(this.props, function (_) {
        return _.profile.targetKey;
      }) === 'wally') {
        var targetValue = this.context.wallyGetTags();

        return _react2.default.createElement(_AdSlot2.default, (0, _extends3.default)({}, this.props, { targetValue: targetValue }));
      }

      return _react2.default.createElement(_AdSlot2.default, this.props);
    }
  }]);
  return AdSlotWithWally;
}(_react.Component);

AdSlotWithWally.contextTypes = {
  wallyGetTags: _propTypes2.default.func
};
exports.default = AdSlotWithWally;