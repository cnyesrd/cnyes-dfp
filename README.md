# cnyes-dfp

A React component to execute Google DFP logic.


![img](https://bytebucket.org/cnyesrd/cnyes-dfp/raw/14b61ff168864541a1a6add690afb56b11976b19/dfp.png)



## Install

  ```
  yarn add https://bitbucket.org/cnyesrd/cnyes-dfp.git#[commit or tag]
  ```



## Usage

### Add DFP provider into your root component

* DFPProvider will provide all necessary DFP methods by context.

```jsx

  // In your App or Page component (root component in project)
  import { DFPProvider } from 'cnyes-dfp';

  render() {
    return (
      <DFPProvider>
        <div>
          ...
        </div>
      </DFPProvider>
    );
  }

```



### Add an AdSlot component with specified profile

```jsx
  import { AdSlot } from 'cnyes-dfp';

  class SomeComponent extends React.Component {
    ...
    render() {
      return (
        <section>
          ...
          <AdSlot
            profile={{
              path: '/1000000/adunit',
              size: ['fluid'],
              waitingFor: 'detail',
              hideOnInitial: true,
              className: 'native-style',
            }}
          />
        </section>
      );
    }
  }
```




## DFPProvider

### Child context

```jsx
  getChildContext() {
    return {
      getIsSlotAdReady: this.getIsSlotAdReady,
      setIsSlotAdReady: this.setIsSlotAdReady,
      getIsComponentMounted: this.getIsComponentMounted,
      setIsComponentMounted: this.setIsComponentMounted,
      refreshAds: this.refreshAds,
      clearAdSlots: this.clearAdSlots,
    };
  }
```


#### getIsSlotAdReady(slotName)
Get the target slot is ready to display or not

- slotName (string): target slot name



#### setIsSlotAdReady(slotName, isReady)
Set the target slot is ready to display or not

- slotName (string): target slot name
- isReady (bool)



#### getIsComponentMounted(componentName)
Get the target component is mounted or not

- componentName (string): target component name



#### setIsComponentMounted(componentName, doRefreshAd)
Set the target component is mounted or not

- componentName (string): target component name
- doRefreshAd (bool): refresh all unrefreshed related ads



#### refreshAds()
Refresh all unrefreshed slots



#### clearAds()
Clear all unrefreshed slots

---


### Props
```jsx
propTypes: {
  /**
   * Enables or disables collapsing of slot divs so that they don't
   * take up any space on the page when there is no ad content to display.
   */
  collapseEmptyDivs: ProTypes.bool, // default false

  /**
   * Event handler for slotRenderEnded event.
   */
  onSlotRenderEnded: PropTypes.func, // default undefined

  /*
    set true if you want to load gpt.js
  */
  loadGPT: PropTypes.bool, // default true

  /*
    disable refresh ads for debug
  */
  disable: PropTypes.bool, //default false
},
```



#### onSlotRenderEnded(provider, event)

- provider: child context from DFPProvider
- event: [GPT event](https://developers.google.com/doubleclick-gpt/reference#googletageventsslotrenderendedevent)


```js
// example
handleSlotRenderEnded = (provider, event) => {
    const adElement = document.getElementById(`${event.slot.getSlotElementId()}-container`);

    switch (event.slot.getAdUnitPath()) {
      case AdProfiles.idleBannerCenter.path: {
        provider.setIsSlotAdReady(AdProfiles.idleBannerCenter.name, !event.isEmpty);
        break;
      }
      default:
        break;
    }

    if (event.isEmpty && adElement) {
      adElement.style.display = 'block';
    }
};
```



## AdSlot

### Props
```js
  // [300, 250], [[300, 250], 'fluid'], ['fluid]
  const SIZE_TYPE = PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.number), PropTypes.string, PropTypes.number]),
  );


  /* if responsive break points are [0, 0] and [1300, 0] ([width, height])
  [
    [
      [0,0],                            // responsive point
      [[1, 1], [650, 60], 'fluid'],     // slot size
    ],
    [
      [1300, 0],                        // responsive point
      [[1, 1], [800, 60], 'fluid']      // slot size
    ]
  ]
  */
  const MULTI_SIZE_TYPE = PropTypes.arrayOf(PropTypes.arrayOf(AD_SIZE_TYPE));


  const ProfilePropType = PropTypes.shape({
    path: PropTypes.string,           // DFP code
    name: PropTypes.string,           // slot name
    size: SIZE_TYPE,                  // slot size
    multiSize: MULTI_SIZE_TYPE,       // responsive size mapping
    multiSizeHandler: PropTypes.func, // responsive handler
    waitingFor: PropTypes.string,     // refresh ads after component didmount
    hideOnInitial: PropTypes.bool,    // set display: none; on initial
    targetKey: PropTypes.string,      // for setting DFP targeting
    outOfPage: PropTypes.bool,        // set true if the slot is outOfPage ad
  });

  propTypes: {
    // Profile data for slot
    profile: ProfilePropType.isRequired,

    // class name
    className: PropTypes.string,

    // Init slot after specific millisecond
    asyncInit: PropTypes.number,

    // use lazyLoading mode
    lazyLoading: PropTypes.bool,

    /**
     * `topOffset` can either be a number, in which case its a distance from the
     * top of the container in pixels, or a string value. Valid string values are
     * of the form "20px", which is parsed as pixels, or "20%", which is parsed
     * as a percentage of the height of the containing element.
     * For instance, if you pass "-20%", and the containing element is 100px tall,
     * then the waypoint will be triggered when it has been scrolled 20px beyond
     * the top of the containing element.
     */
    lazyLoadingTopOffset: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

    /**
     * `bottomOffset` is like `topOffset`, but for the bottom of the container.
     */
    lazyLoadingBottomOffset: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

    // DFP setTargeting
    targetValue: PropTypes.arrayOf(PropTypes.string),

  },
```



## AdSlotWithWally

HOC for AdSlot

This component will automatically get wally tags from Wally's context for rendering AdSlot.

### Integrate with Wally

```
// AdSlotWithWally.js

static contextTypes = {
  wallyGetTags: PropTypes.func,
};
```

`<AdSlotWithWally />` 接收一個叫做 `wallyGetTags` 的 context，所以需要整合 Wally 的專案必須實作一個外層 Component 來提供這個 context，例如 fe-fund 的 [`Wally.js`](https://bitbucket.org/cnyesrd/fe-fund/pull-requests/400/wally-fe-fund/diff#chg-src/components/DFP/Wally.js) 或是 fe-crypto 的 [`withWally.js`](https://bitbucket.org/cnyesrd/fe-crypto/pull-requests/80/gs-8883-gs-8950-wally/diff#chg-src/decorators/withWally.js)

```
// Wally.js or withWally.js

getChildContext() {
  return {
    wallyGetTags: this.getTags,
  };
}
```

#### 實作流程：

![img](https://bitbucket.org/cnyesrd/cnyes-dfp/raw/55f3082c5b0f83da4e8ff351aa75b9b8e4454a45/wally.png)

## Reference
https://github.com/krasimir/webpack-library-starter



## Roadmap

1. One DFP logic for all projects.

2. Loading placeholder

3. Use intersection observer

4. E2E test to check Ads status
