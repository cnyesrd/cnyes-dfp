/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import idx from 'idx';
import AdSlot from './AdSlot';

class AdSlotWithWally extends Component {
  static contextTypes = {
    wallyGetTags: PropTypes.func,
  };

  render() {
    if (idx(this.props, _ => _.profile.targetKey) === 'wally') {
      const targetValue = this.context.wallyGetTags();

      return <AdSlot {...this.props} targetValue={targetValue} />;
    }

    return <AdSlot {...this.props} />;
  }
}

export default AdSlotWithWally;
